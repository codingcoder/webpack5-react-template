import React from 'react';
import {
    RecoilRoot
} from 'recoil';

import RouterConfig from '@/routes';


type IProps = {
    
};

const App: React.FC<IProps> = () => {
    return (
        <RecoilRoot>
            <RouterConfig></RouterConfig>
        </RecoilRoot>
    );
}

export default App;