import React from 'react';
import {
    useNavigate
} from 'react-router-dom';

type IProps = {

}

const Home: React.FC<IProps> = () => {
    const navigate = useNavigate()
    return (
        <div>
            这是Home

            <button onClick={() => {
                navigate('/user/register')
            }}>跳转register</button>
        </div>
    )
}

export default Home