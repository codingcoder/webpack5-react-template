import React from 'react';
import { userState } from '@/store/user';
import {
    useRecoilState
} from 'recoil';

type IProps = {

}

const Login: React.FC<IProps> = () => {
    const [user, setUser] = useRecoilState(userState)
    return (
        <div>
            <div>username: {user.username}</div>
            <div>sex: {user.sex}</div>

            <button onClick={() => {
                setUser({
                    username: '213',
                    sex: '女'
                })
            }}>点击我</button>
        </div>
    )
}

export default Login