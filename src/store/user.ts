import {
    atom,
    selector
} from 'recoil';

const initialState: UserGlobal.User = {
    username: '小wang',
    sex: '男'
}

export const userState = atom({
    key: 'userState',
    default: initialState
})

