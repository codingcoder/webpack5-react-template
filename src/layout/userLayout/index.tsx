import React from 'react';
import {
    Outlet
} from 'react-router-dom';

type IProps = {

}

const UserLayout: React.FC<IProps> = () => {
    return (
        <div>
            UserLayout
            <Outlet />
        </div>
    )
}

export default UserLayout