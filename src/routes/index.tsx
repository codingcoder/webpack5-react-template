import React from 'react';
import {
    HashRouter as Router,
    Routes,
    Route
} from 'react-router-dom';

import Home from '@/pages/home';
import Login from '@/pages/login';
import Register from '@/pages/register';

import UserLayout from '@/layout/userLayout';

const RouterConfig: React.FC = () => {
    return (
        <Router>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/home" element={<Home />} />
                <Route path="user" element={<UserLayout />}>
                    <Route path="login" element={<Login />} />
                    <Route path="register" element={<Register />} />
                </Route>
            </Routes>
        </Router>      
    )
}

export default RouterConfig