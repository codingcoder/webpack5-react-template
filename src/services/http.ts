import axios, { AxiosRequestConfig } from "axios";
import { addPending, removePending } from './pending';

// 设置loading
export const Loading = (loading: boolean) => {
    
}

// 创建请求实例
const instance = axios.create({
    baseURL: process.env.REACT_APP_BASE_URL,
    timeout: 5000,
    headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        'X-Access-Token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MTY3MzQ2MTUsInVzZXJuYW1lIjoiYWRtaW4ifQ.r8JZwm90S9uGbWcmA3CfL1KeMTAyZB_r7NZGoIy54mQ'
    }
});

// 添加请求拦截器
instance.interceptors.request.use((config: AxiosRequestConfig) => {
    removePending(config)
    addPending(config)
    // 发送请求之前做些什么
    Loading(true)
    return config;
}, (err) => {
    // 对请求错误做些什么
    Loading(false)
    return Promise.reject(err);
});

// 添加响应拦截器
instance.interceptors.response.use((response) => {
    // 对响应数据做些什么
    removePending(response)
    Loading(false)
    return response.data;
}, (err) => {
    // 对响应错误做些什么
    Loading(false)
    return Promise.reject(err);
});


export default instance



