declare namespace RouteGlobal {
    interface Routes {
        path: string;
        component: React.ComponentType<any>;
        exact?: boolean;
        redirect?: string;
        children?: Routes []
    }
}
