declare namespace Axios {
    interface Article {
        id: number
        title: string
        body: string
        userId?: number
    }
}
