const {
    merge
} = require('webpack-merge');
const common = require('./webpack.common.js');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CompressionPlugin = require('compression-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const webpack = require('webpack');

const env = require("../config/prod.env");
module.exports = merge(common, {
    mode: 'production',
    plugins: [
        // 分析build包的树状图
        new BundleAnalyzerPlugin(),
        new webpack.DefinePlugin({
            "process.env": env,
        }),
        // 开启gzip压缩
        new CompressionPlugin(),
        // 优化和压缩css
        new CssMinimizerPlugin()
    ],
    optimization: {
        // 拆分chunk
        splitChunks: {
            chunks: 'all',
            name: 'vendor',
            cacheGroups: {
                'echarts.vendor': {
                    name: 'echarts.vendor',
                    priority: 40,
                    test: /[\\/]node_modules[\\/](echarts|zrender)[\\/]/,
                    chunks: "all",
                },
                'lodash': {
                    name: 'lodash',
                    chunks: 'async',
                    test: /[\\/]node_modules[\\/]lodash[\\/]/,
                    priority: 40,
                },
                'async-common': {
                    chunks: 'async',
                    minChunks: 2,
                    name: 'async-commons',
                    priority: 30,
                },
                'commons': {
                    name: 'commons',
                    chunks: 'all',
                    minChunks: 2,
                    priority: 20,
                }
            }
        }
    }
})