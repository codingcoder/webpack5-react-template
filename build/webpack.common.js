const path = require('path');
const chalk = require('chalk');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    mode: 'development',
    entry: path.resolve(__dirname, '../src/index.tsx'),
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename: 'static/js/[name].[contenthash:8].js',
        clean: true
    },
    resolve: {
        extensions: ['.js', '.jsx', '.ts', '.tsx', '.json'],
        alias: {
            '@': path.resolve(__dirname, '../src')
        }
    },
    // 扩展webpack功能
    plugins: [
        // 生成html文件，并引入bundle.js
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, '../public/index.html'),
            filename: 'index.html'
        }),
        // 进度条
        new ProgressBarPlugin({
            format: `:msg [:bar] ${chalk.green.bold(":percent")}(:elapsed s)`
        }),
        // 抽离css
        new MiniCssExtractPlugin({
            filename: 'static/css/[name].[contenthash].css'
        })
    ],
    module: {
        // loader 用于对模块的源代码进行转换
        rules: [{
                test: /\.(css|scss|sass)$/,
                use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader", {
                    loader: "postcss-loader",
                    options: {
                        postcssOptions: {
                            plugins: [
                                "autoprefixer"
                            ]
                        }
                    }
                }] // //从右向左解析
            },
            {
                test: /\.(png|jpe?g|gif|svg|ico)(\?.*)?$/, //加载图片资源
                loader: "url-loader",
                type: 'javascript/auto', //解决asset重复
                options: {
                    esModule: false, //解决html区域,vue模板引入图片路径问题
                    limit: 1000,
                    name: "static/img/[name].[hash:7].[ext]",
                },
            },
            {
                test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/, //加载视频资源
                loader: "url-loader",
                options: {
                    limit: 10000,
                    name: "static/media/[name].[hash:7].[ext]",
                },
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/i, //加载字体资源
                loader: "url-loader",
                options: {
                    limit: 10000,
                    name: "static/fonts/[name].[hash:7].[ext]",
                },
            },
            {
                test: /(\.jsx|\.js|\.ts|\.tsx)$/,
                use: ["babel-loader"],
                exclude: /node_modules/,
            },
            {
                test: /\.html$/i,
                loader: "html-loader",
                options: {
                    esModule: false,
                }
            },
            {
                test: /\.tsx?$/,
                loader: "ts-loader",
                exclude: /node_modules/
            }
        ]
    }
}